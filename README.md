# yaru-plus-git

Yaru++ – Elegant and modern third-party icons theme, based on Canonical’s Yaru

https://github.com/Bonandry/yaru-plus

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/themes-and-icons/yaru-plus-git.git
```

